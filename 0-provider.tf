##---------------------------------------------------------------------------------
## Aqui especificamos el provider y la versión requerida de Terraform
##---------------------------------------------------------------------------------

provider "aws" {
  region = "us-east-1"
}

terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.56"
    }
  }
}
