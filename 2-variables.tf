##---------------------------------------------------------------------------------
## Variables con el nombre del cluster y de la vpc
##---------------------------------------------------------------------------------

variable "cluster_name" {
  type        = string
  description = "Nombre del cluster EKS"
  default     = "ops-wordpress-dev"
}

variable "vpc_name" {
  type        = string
  description = "Nombre del VPC"
  default     = "wordpress-project-dev"
}

variable "vpc_cidr" {
  type        = string
  description = "Rango de VPC"
  default     = "10.10.0.0/16"
}