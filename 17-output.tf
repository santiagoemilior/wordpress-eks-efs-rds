// -------------------------------------------------------------------------
// ID del VPC
// -------------------------------------------------------------------------

output "vpc_id" {
  value       = aws_vpc.main.id
  description = "VPC ID"
}

// -------------------------------------------------------------------------
// Nombre del cluster
// -------------------------------------------------------------------------

output "cluster_name" {
  description = "Nombre de nuestr cluster"
  value       = aws_eks_cluster.wordpress.name
}

// -------------------------------------------------------------------------
// URL del repositorio en codecommit
// -------------------------------------------------------------------------

output "repository_clone_url_ssh" {
  description = "URL que usaremos para interactuar con nuestro repositorio"
  value       = aws_codecommit_repository.wordpress.clone_url_ssh
}