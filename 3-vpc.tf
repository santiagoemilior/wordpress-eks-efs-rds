##---------------------------------------------------------------------------------
## VPC y su cidr
##---------------------------------------------------------------------------------

resource "aws_vpc" "main" {
  cidr_block = var.vpc_cidr

  enable_dns_support   = true
  enable_dns_hostnames = true

  tags = {
    Name = "${var.cluster_name}-vpc-final"
  }
}

# resource "aws_vpc_peering_connection" "peering_con" {
#   peer_vpc_id = "vpc-0db3509c637d04138"
#   vpc_id      = aws_vpc.main.id
#   peer_region = "us-east-1"
# }

# resource "aws_vpc_peering_connection_accepter" "aceptacion" {
#   vpc_peering_connection_id = aws_vpc_peering_connection.peering_con.id
#   auto_accept               = true
# }

# resource "aws_vpc_peering_connection_options" "this" {
#   vpc_peering_connection_id = aws_vpc_peering_connection.peering_con.id
#   accepter {
#     allow_remote_vpc_dns_resolution = true
#   }
# }
# data "aws_route_tables" "main" {
#   vpc_id = aws_vpc.main.id
# }

# data "aws_vpc" "aceptador" {
#   id = "vpc-0db3509c637d04138"
# }


# data "aws_route_tables" "aceptador" {
#   vpc_id = "vpc-0db3509c637d04138"
# }

# resource "aws_route" "main" {
#   count                     = length(data.aws_route_tables.main.ids)
#   route_table_id            = data.aws_route_tables.main.ids[count.index]
#   destination_cidr_block    = data.aws_vpc.aceptador.cidr_block
#   vpc_peering_connection_id = aws_vpc_peering_connection.peering_con.id
# }

# resource "aws_route" "aceptador" {
#   count                     = length(data.aws_route_tables.aceptador.ids)
#   route_table_id            = data.aws_route_tables.aceptador.ids[count.index]
#   destination_cidr_block    = aws_vpc.main.cidr_block
#   vpc_peering_connection_id = aws_vpc_peering_connection.peering_con.id
# }