// -------------------------------------------------------------------------
// RDS de aurora mysql para nuestro wordpress site
// encriptada con nuestra llave kms
// -------------------------------------------------------------------------

resource "random_password" "db_password" {
  length = 16
}
resource "aws_db_subnet_group" "aurora-subnet" {
  name       = "${var.cluster_name}-subnet-group-final"
  subnet_ids = [aws_subnet.private_db_1a.id, aws_subnet.private_db_1b.id]

  tags = {
    Name = "${var.cluster_name}-subnet-group-final"
  }
}

module "cluster" {
  source = "terraform-aws-modules/rds-aurora/aws"

  name                        = "${var.cluster_name}-aurora-db-mysql"
  engine                      = "aurora-mysql"
  instance_class              = "db.t3.medium"
  database_name               = "wordpress" # <--- Nombre de la base de datos
  master_username             = "admin"     # <--- Nombre del usuario de la base de datos
  master_password             = "password"  # <--- Clave del usuario de la base de datos
  manage_master_user_password = false
  skip_final_snapshot         = true
  vpc_security_group_ids      = [aws_security_group.rds.id]
  instances = {
    one = {
      instance_class = "db.t3.medium"
    }
  }

  vpc_id               = aws_vpc.main.id
  db_subnet_group_name = aws_db_subnet_group.aurora-subnet.name

  storage_encrypted   = true
  apply_immediately   = true
  kms_key_id          = module.kms.key_arn
  monitoring_interval = 10
}

# resource "aws_secretsmanager_secret" "db_secret" {
#   kms_key_id = module.kms.key_id
#   name = "db-credentials"
#   description = "Secreto de credenciales de DB"
# }

# resource "aws_secretsmanager_secret_version" "db-credenciales" {
#   secret_id = aws_secretsmanager_secret.db_secret
#   secret_string = <<EOF
# {
#   "Username": "${module.cluster.aws_rds_cluster.master_username}",
#   "Password": "${random_password.db_password.result}",
#   "Engine": "${module.cluster.aws_rds_cluster.this.engine}", 
#   "Host": "${module.cluster.aws_rds_cluster_endpoint.cluster_endpoint_identifier}",
#   "Port": "${module.cluster.aws_rds_cluster.port}"
# }
# EOF
# }
