// -------------------------------------------------------------------------
// Addon de EFS, favor cargar el de EBS primero y luego este
// -------------------------------------------------------------------------

resource "aws_eks_addon" "efs_csi_driver" {
  cluster_name             = aws_eks_cluster.wordpress.name
  addon_name               = "aws-efs-csi-driver"
  addon_version            = "v1.7.0-eksbuild.1"
  service_account_role_arn = aws_iam_role.eks_efs_csi_driver.arn
}
