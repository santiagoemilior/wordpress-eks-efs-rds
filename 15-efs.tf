// -------------------------------------------------------------------------
// Usando un module predeterminado creaos un EFS filesystem encryptado con 
// nuestra llave kms y desplegado en nuestras subnets privadas
// -------------------------------------------------------------------------

module "efs" {
  source = "terraform-aws-modules/efs/aws"

  name        = "${var.cluster_name}-wp-efs"
  encrypted   = true
  kms_key_arn = module.kms.key_arn

  performance_mode = "generalPurpose"
  throughput_mode  = "bursting"
  attach_policy    = false

  mount_targets = {
    "us-east-1a" = {
      subnet_id = aws_subnet.private_us_east_1a.id
    }
    "us-east-1b" = {
      subnet_id = aws_subnet.private_us_east_1b.id
    }
  }

  security_group_description = "EFS security group"
  security_group_vpc_id      = aws_vpc.main.id
  security_group_rules = {
    vpc = {
      description = "NFS ingress from VPC private subnets"
      cidr_blocks = ["10.10.0.0/16"]
    }
  }

  enable_backup_policy = true

}
