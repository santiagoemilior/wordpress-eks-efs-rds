// -------------------------------------------------------------------------
// Security groups de nuestros servicios
// -------------------------------------------------------------------------

resource "aws_security_group" "efs" {
  name        = "efs-sg"
  description = "EFS access"
  vpc_id      = aws_vpc.main.id

  tags = {
    Name = "EFS-sg"
  }
}

resource "aws_security_group_rule" "efs-out" {
  type        = "egress"
  from_port   = 0
  to_port     = 0
  protocol    = "-1"
  cidr_blocks = ["0.0.0.0/0"]

  security_group_id = aws_security_group.efs.id
}

resource "aws_security_group_rule" "efs-in" {
  type              = "ingress"
  from_port         = 2049
  to_port           = 2049
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.efs.id
}

resource "aws_security_group" "rds" {
  name        = "rds-sg"
  description = "RDS access"
  vpc_id      = aws_vpc.main.id

  tags = {
    Name = "RDS-sg"
  }
}

resource "aws_security_group_rule" "rds-out" {
  type        = "egress"
  from_port   = 0
  to_port     = 0
  protocol    = "-1"
  cidr_blocks = ["0.0.0.0/0"]

  security_group_id = aws_security_group.rds.id
}

resource "aws_security_group_rule" "rds-in" {
  type              = "ingress"
  from_port         = 3306
  to_port           = 3306
  protocol          = "tcp"
  cidr_blocks       = ["10.10.0.0/16"]
  security_group_id = aws_security_group.rds.id
}