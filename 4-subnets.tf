// -------------------------------------------------------------------------
// Aca creamos las subnets que usaremos
// -------------------------------------------------------------------------

data "aws_availability_zones" "zonas" {}

resource "aws_subnet" "private_us_east_1a" {
  vpc_id            = aws_vpc.main.id
  cidr_block        = cidrsubnet(var.vpc_cidr, 4, 0)
  availability_zone = data.aws_availability_zones.zonas.names[0]

  tags = {
    "Name"                                      = "${var.cluster_name}-private-us-east-1a"
    "kubernetes.io/role/internal-elb"           = "1"
    "kubernetes.io/cluster/${var.cluster_name}" = "owned"
  }
}

resource "aws_subnet" "private_us_east_1b" {
  vpc_id            = aws_vpc.main.id
  cidr_block        = cidrsubnet(var.vpc_cidr, 4, 1)
  availability_zone = data.aws_availability_zones.zonas.names[1]

  tags = {
    "Name"                                      = "${var.cluster_name}-private-us-east-1b"
    "kubernetes.io/role/internal-elb"           = "1"
    "kubernetes.io/cluster/${var.cluster_name}" = "owned"
  }
}

resource "aws_subnet" "public_us_east_1a" {
  vpc_id                  = aws_vpc.main.id
  cidr_block              = cidrsubnet(var.vpc_cidr, 4, 2)
  availability_zone       = data.aws_availability_zones.zonas.names[0]
  map_public_ip_on_launch = true

  tags = {
    "Name"                                      = "${var.cluster_name}-public-us-east-1a"
    "kubernetes.io/role/elb"                    = "1"
    "kubernetes.io/cluster/${var.cluster_name}" = "owned"
  }
}

resource "aws_subnet" "public_us_east_1b" {
  vpc_id                  = aws_vpc.main.id
  cidr_block              = cidrsubnet(var.vpc_cidr, 4, 3)
  availability_zone       = data.aws_availability_zones.zonas.names[1]
  map_public_ip_on_launch = true

  tags = {
    "Name"                                      = "${var.cluster_name}-public-us-east-1b"
    "kubernetes.io/role/elb"                    = "1"
    "kubernetes.io/cluster/${var.cluster_name}" = "owned"
  }
}

resource "aws_subnet" "private_db_1a" {
  vpc_id            = aws_vpc.main.id
  cidr_block        = cidrsubnet(var.vpc_cidr, 4, 4)
  availability_zone = data.aws_availability_zones.zonas.names[0]

  tags = {
    "Name" = "${var.cluster_name}-db-private-us-east-1a"
  }
}

resource "aws_subnet" "private_db_1b" {
  vpc_id            = aws_vpc.main.id
  cidr_block        = cidrsubnet(var.vpc_cidr, 4, 5)
  availability_zone = data.aws_availability_zones.zonas.names[1]

  tags = {
    "Name" = "${var.cluster_name}-db-private-us-east-1b"
  }
}
