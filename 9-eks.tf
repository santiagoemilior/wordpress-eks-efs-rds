// -------------------------------------------------------------------------
// Aqui creamos primero el rol para nuestro Cluster y luego creamos el cluster
// -------------------------------------------------------------------------

resource "aws_iam_role" "wordpress" {
  name = "${var.cluster_name}-wordpress-role"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "eks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "wordpress_amazon_eks_cluster_policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  role       = aws_iam_role.wordpress.name
}

resource "aws_eks_cluster" "wordpress" {
  name     = var.cluster_name
  version  = "1.28"
  role_arn = aws_iam_role.wordpress.arn

  vpc_config {
    subnet_ids = [
      aws_subnet.private_us_east_1a.id,
      aws_subnet.private_us_east_1b.id,
      aws_subnet.public_us_east_1a.id,
      aws_subnet.public_us_east_1b.id
    ]
  }

  depends_on = [aws_iam_role_policy_attachment.wordpress_amazon_eks_cluster_policy]
}

