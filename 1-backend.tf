##---------------------------------------------------------------------------------
## Aca usamos un S3 bucket previamente creado para almacenar nuestro archivo de estado.
## El bucket debe ser creado previamente y se debe habilitar la función de Versioning.
##---------------------------------------------------------------------------------

terraform {
  backend "s3" {
    bucket = "wordpress-backend-90235485"
    key    = "remote.tfstate"
    region = "us-east-1"
  }
}